* Rationale

This repository provides a template for working with datasets
involving household expenditure data; the idea is to create a uniform
interface that can then be used "downstream" by an analysis
package such as https://pypi.org/project/CFEDemands/.

* Prerequisites

  Things you'll want to have set up in advance include:

  - A =bitbucket.org= account.  You'll be much happier if you upload
    your public =ssh-key= to =bitbucket.org= for authentication
    purposes.

  - The =git= binary on your local computer.

  - Git Large File Storage (=git-lfs=); see
    https://confluence.atlassian.com/bitbucket/use-git-lfs-with-bitbucket-828781636.html. 

  - =emacs=, for working with the =org-mode= files used in this
    project. 


* Work-flow

  Here's the intended work-flow for using this skeleton:
** Initial Setup
  1. *Identify a dataset* which includes detailed household expenditures
     you're interested in.  Examples include the various LSMS datasets
     that the World Bank collects
     (http://iresearch.worldbank.org/lsms/lsmssurveyFinder.htm).

  2. *Fork* https://bitbucket.org/eligon/skeletonexpendituredataset,
     changing the name of the repository to incorporate a reference to
     the dataset (e.g., SkeletonExpenditureDataset -> UgandaLSMS2011).

  3. *Clone* your fork to your local computer.

  4. *Add a remote* by executing the following in your new git repo:
     #+begin_src sh
     git remote add skeleton git@bitbucket.org:eligon/skeletonexpendituredataset.git
     #+end_src
     You'll only need to do this once; this will make it possible to pull changes to the template into
     your new dataset-specific repository.  

  5. To merge in changes from skeleton, first make sure you've committed any
     changes you've made to your local repository.  Then the recommended process is to:
     #+begin_src sh
     git fetch skeleton          # Fetch any changes from the original skeleton repo
     git checkout master         # Make sure you're on your master branch
     git rebase skeleton/master  # Write your own changes on top of changes to skeleton.
     #+end_src
     The frequency with which you do this is up to you; it's only
     necessary if you want to advantage of new documentation,
     features, or bug-fixes to =SkeletonExpenditureDataset=.

** Adding New Data
  1. Obtain a copy of the data you're interested in, and put it in the
     directory =./OriginalData=.  You may want to review
     [[file:OriginalData/README.org::*Instructions][./OriginalData/README.org:Instructions]].

  2. Add the data archive to the repository.  For example, if it's in an archive
     called =foo.zip=, you'd do something like
     #+begin_src sh
     cd ./OriginalData
     git add foo.zip
     git commit -m"Added original archive foo.zip to repository."
     #+end_src

  3. Carefully document the original data.  Where did it come from?


** Creating Analysis Datasets
   
   For demand analysis you'll want to collect several distinct sorts
   of data.  These data and any transformations to them should be
   documented in a file [[./data_extraction.org]]

   Perhaps most obviously these will include data on expenditures,
   probably at the level of the household (but possibly at the level
   of an individual or other level of aggregation).


