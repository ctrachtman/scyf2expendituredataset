* Kenyan LPS SCY F2

** Expenditures Label from Round 3 of KLPS SCY F2

#+name: expenditure_codes
| Code    | Expenditure Item         | Aggregate Label          | 
|---------+--------------------------|--------------------------|
| se13_4  | Rice                     | Rice                     |
| se13_7  | Irish Potato             | Irish Potato             |
| se13_9  | Wheat Flour              | Wheat Flour              |
| se13_10 | Plantain                 | Plantain                 |
| se13_13 | Beans                    | Beans                    |
| se13_15 | Green grams              | Green grams              |
| se13_16 | Tomatoes                 | Tomatoes                 |
| se13_17 | Onions                   | Onions                   |
| se13_18 | Kale                     | Kale                     |
| se13_19 | Cabbage                  | Cabbage                  |
| se13_23 | Beef                     | Beef                     |
| se13_25 | Chicken, poultry         | Chicken, poultry         |
| se13_28 | Eggs                     | Eggs                     |
| se13_31 | Cooking fat              | Cooking fat              |
| se13_32 | Sugar                    | Sugar                    |
| se13_35 | Tea, coffee              | Tea, coffee              |
| se13_36 | Bread, biscuits, cakes   | Bread, biscuits, cakes   |
| se13_41 | Watermelon               | Watermelon               |
| se13_42 | Bananas                  | Bananas                  |
| se13_43 | Oranges and other citrus | Oranges and other citrus |
| se13_44 | Pineapple                | Pineapple                |
| se13_45 | Avocado                  | Avocado                  |
| se13_46 | Mango                    | Mango                    |

#+name: expenditure_dictionary
#+BEGIN_SRC python :preamble "# -*- coding: utf-8 -*-" :noweb no-export :results output :tangle test.py :var X=expenditure_codes :colnames no
  from collections import defaultdict
  import pickle

  d=defaultdict(list)
  for r in X[1:]:
      for i in range(len(X[0])): 
          d[X[0][i]]+=[r[i]]

  ed=defaultdict(list)
  for (l,n) in zip(d['Aggregate Label'],d['Code']):
      ed[l]+=[n]

  with open('./var/expenditure_codes.pickle','wb') as f:
      pickle.dump(ed,f)
#+END_SRC

#+results: expenditure_dictionary



** First Stage Dataset Construction

#+name: klps_stage1
#+begin_src python :noweb no-export :results output :tangle klps_stage1.py 
import pandas as pd
import numpy as np
food_expenditures = pd.read_stata("./OriginalData/SCY_F2_consumption_forNeedinessIndex_20180206.dta")
food_expenditures['name']=food_expenditures['name'].apply(lambda x: x.capitalize())
df_food=food_expenditures.copy()
df_food.columns.name = None
hh_compositions = pd.read_stata("./OriginalData/SCY_F2_household_covars_20180206.dta")

df_pivot = df_food.pivot(index='pupid', columns='name', values='cnsp')

df_pivot =df_pivot.reset_index()
df_pivot.columns.name = None

df=pd.merge(df_pivot,hh_compositions,how="inner")

#df.rename(columns={"pupid":"j"}, inplace=True)
#df.set_index(["j"], inplace=True)

df["pupsex"]=1-df["female"]
df["adults"]=1+df["s4_2hhadults"]

df.rename(columns={"s4_10hhchildren":"children"}, inplace=True)

df["pupage"]=2016-df["yob"]

df["hhsize"]=df["adults"]+df["children"]

df.to_pickle("./var/scyf2.df")

food_exp = pd.concat([df.loc[:, "region"],df.loc[:, "Avocado":"Wheat flour"]],axis=1)

food_exp.to_pickle("./var/food_exp.df")
hh_comp = pd.concat([df.loc[:, "children":"region"],df.loc[:, "pupsex":"hhsize"]],axis=1)


hh_comp.to_pickle("./var/hh_comp.df")
#+end_src

#+results: klps_stage1




** Second Stage Dataset Construction

*************** TODO Edit text below
*************** TODO END

The following sets up a dataframe from Kenyan Life Panel Survey YF2
data; it relies on code from the above sections specifically
=klps_stage1.py=, which creates preliminary datasets
=scyf2expendituredataset/var/scyf2.df=,
=scyf2expendituredataset/var/food_exp.df=,
and =scyf2expendituredataset/var/hh_comp= used below
to create a pickled pd.DataFrame =scyf2expendituredataset/var/klps.df=; 
also =test.py= which creates 
=scyf2expendituredataset/var/expenditure_codes.pickle=, providing a mapping of
codes for different expenditure items into particular food categories.

Output from this routine should include a pair of =pd.DataFrames=
indexed by =(j,t)=.  The  first is called =y=, with columns
corresponding to log expenditures (zero expenditures should be set to
=NaN=).    The second is called =z=, with columns corresponding to
various household characteristics to be controlled for in the demand
estimation.  

Other inputs to the routine include information on how markets are to
be defined; conceptually,  these are areas (ordinarily geographical)
within which we expect the Law of One Price to hold,  at least
approximately.  This involves specifying a variable in =klps.df=
which gives an indication of what market a given  household resides
in.  Examples might include region, or a rural/urban identifier.  If
empty, it's assumed that  all households live in a common market.

#+name: market
| Region |

A final input is a list of variables in =klps.df= of household
characteristics, to be controlled for in the demand regressions.  This
will typically include information on household composition.  Note
that this list must /not/ include the variables which define the market.

#+name: controls
| log HSize | Adults | Kids | Pupil sex | Pupil age |

#+name: scyf2_data
#+begin_src python :noweb no-export :var MARKET=market[0] :var CONTROLS=controls[0] :results output raw table :tangle scyf2_data.py
  import pandas as pd
  import numpy as np
  import sys
  sys.path.append('./var/')
  import cfe.estimation as nd

  def scyf2_data(MARKET=MARKET,CONTROLS=CONTROLS,DataDir='./var/'):
      """Return log expenditures y, household characteristics z, and prices.
      """

      assert len(set(MARKET).intersection(CONTROLS))==0, "Controls must not include market."

      exps=pd.read_pickle(DataDir+'/expenditure_codes.pickle')

      try:
          df=pd.read_pickle(DataDir+'/scyf2.df')
          #prices=pd.read_pickle(DataDir+'/food_prices.df')
      except IOError:
          print("Need to build scyf2.df (using code in scyf2expendituredataset/MANIFEST_SCYF2.org).")
    

      expdf=df.loc[:,exps.keys()] #nd.group_expenditures(df,exps)

      expdf=expdf.replace(0,np.nan) # Zeros to NaN

      # Drop goods where too few observations to reliably estimate covariance matrix
      expdf=nd.drop_columns_wo_covariance(expdf,min_obs=30,VERBOSE=False)

      # Fix up some variables used as characteristics
      expdf.rename(columns={'region':'Region'},inplace=True)
      mydf=expdf.copy()


      # Updated controls
      mydf['HHSize']=df['hhsize']
      mydf['HHSize'].replace(to_replace=[0],value=[np.NaN],inplace=True)
      mydf['log HSize']=np.log(mydf['HHSize'])
      mydf['Adults']=df['adults']
      mydf['Kids']=df['children']
      mydf['Pupil sex']=df['pupsex']
      mydf['Pupil age']=df['pupage']
      mydf['Region']=df['region']
      mydf['pupid']=df['pupid']
      mydf['t']=2016
   
      if 'Region' in CONTROLS: # Make dummies
          pd.get_dummies(mydf, prefix=['Region'],columns=['Region'])
          mydf.drop(['Region'], axis=1)

      # Fix indices
      mydf.rename(columns={'pupid':'j'},inplace=True)
      mydf.set_index(['j', 't'])
      #mydf.index.set_names(['j','t'],inplace=True)

      mydf.sort_index(inplace=True)

      mydf.reset_index(inplace=True)
      if len(MARKET[0]):
          mydf['m']=mydf[MARKET]
      else:
          mydf['m']=1

      mydf = mydf.set_index(['j','t','m'])

      y=np.log(mydf[expdf.columns])
      z=mydf[CONTROLS]

      return y,z

  y,z = scyf2_data()

#+end_src

#+results: scyf2_data


